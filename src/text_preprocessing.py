import pandas as pd
from os import path
import spacy
import argparse
from tqdm import tqdm


class TextPreprocessing:
    """
    Class TextPreprocessing
    Use to preprocess the textual data contains in 'designation' column from rakuten dataset
    """
    def __init__(self, data_path, use_lemmatizer=False):
        """
        :param data_path: path to the folder which contain the training and testing set as csv
        :param use_lemmatizer: [boolean] True implies that spacy.lemmatizer will be used during
        the preprocessing
        """
        self.spacy_nlp = spacy.load("fr_core_news_sm")
        self.data_path = data_path
        self.path_x_train = data_path + "X_train.csv"
        self.path_y_train = data_path + "Y_train.csv"
        self.path_x_test = data_path + "X_test.csv"
        self.use_lemmatizer = use_lemmatizer

    @staticmethod
    def normalize_accent(string):
        """
        Remove accents
        """
        string = string.replace('á', 'a')
        string = string.replace('â', 'a')
        string = string.replace('é', 'e')
        string = string.replace('è', 'e')
        string = string.replace('ê', 'e')
        string = string.replace('ë', 'e')
        string = string.replace('î', 'i')
        string = string.replace('ï', 'i')
        string = string.replace('ö', 'o')
        string = string.replace('ô', 'o')
        string = string.replace('ò', 'o')
        string = string.replace('ó', 'o')
        string = string.replace('ù', 'u')
        string = string.replace('û', 'u')
        string = string.replace('ü', 'u')
        string = string.replace('ç', 'c')
        return string

    def remove_punct_stop_and_lemmatize(self, string):
        """
        Remove punctuation and stop words
        """
        spacy_tokens = self.spacy_nlp(string)
        if self.use_lemmatizer:
            string_tokens = [token.lemma_ for token in spacy_tokens if not token.is_punct and not token.is_stop]
        else:
            string_tokens = [token.orth_ for token in spacy_tokens if not token.is_punct and not token.is_stop]
        return " ".join(string_tokens)

    def string_processing(self, string):
        """
        lower the string and remove accent, punctuation and stop words
        """
        string = string.lower()
        string = TextPreprocessing.normalize_accent(string)
        string = self.remove_punct_stop_and_lemmatize(string)
        return string

    @staticmethod
    def remove_nan_values(dataframe):
        """
        the string processing can result in the creation of some NaN values
        this occured when the initial description only contains stop words
        for instance "entre toi et nous"
        On 85k training instances, only 10 exemples have this characterics,
        so we simply replace their designation by "" string
        """
        dataframe.fillna(value="", inplace=True)

    def load_raw_data(self):
        """
        Load the raw data : x_train, y_train, x_test
        in panda dataframe
        """
        x_train = pd.read_csv(self.path_x_train)
        y_train = pd.read_csv(self.path_y_train)
        x_test = pd.read_csv(self.path_x_test)
        return x_train, y_train, x_test

    def load_clean_dataset(self, clean_again=False):
        """
        if datasets have already been cleaned, return corresponding panda dataframe
        else clean the dataset, saved them as csv files and return dataframe

        use clean_again=True to re-clean the dataset even if it has been done already
        """

        if self.use_lemmatizer and \
           path.exists(self.data_path + 'train_clean_with_lemmatizer.csv') and \
           path.exists(self.data_path + 'test_clean_with_lemmatizer.csv') and \
           not clean_again:
            return pd.read_csv(self.data_path + 'train_clean_with_lemmatizer.csv'), \
                   pd.read_csv(self.data_path + 'test_clean_with_lemmatizer.csv')

        if not self.use_lemmatizer and \
           path.exists(self.data_path + 'train_clean_no_lemmatizer.csv') and \
           path.exists(self.data_path + 'test_clean_no_lemmatizer.csv') and \
           not clean_again:
            return pd.read_csv(self.data_path + 'train_clean_no_lemmatizer.csv'), \
                   pd.read_csv(self.data_path + 'test_clean_no_lemmatizer.csv')

        x_train, y_train, x_test = self.load_raw_data()
        train_clean = pd.DataFrame(columns=["string", "label"])
        test_clean = pd.DataFrame(columns=["string"])

        tqdm.pandas()
        print("Training set cleaning...")
        train_clean["string"] = x_train["designation"].progress_apply(lambda s: self.string_processing(s))
        train_clean["label"] = y_train["prdtypecode"]
        self.remove_nan_values(train_clean)

        print("Test set cleaning ...")
        test_clean["string"] = x_test["designation"].progress_apply(lambda s: self.string_processing(s))
        self.remove_nan_values(test_clean)

        if self.use_lemmatizer:
            train_clean.to_csv(self.data_path + 'train_clean_with_lemmatizer.csv', index=False)
            test_clean.to_csv(self.data_path + 'test_clean_with_lemmatizer.csv', index=False)
        else:
            train_clean.to_csv(self.data_path + 'train_clean_no_lemmatizer.csv', index=False)
            test_clean.to_csv(self.data_path + 'train_clean_no_lemmatizer.csv', index=False)

        return train_clean, test_clean


if __name__ == "__main__":
    # SCRIPT TO PREPROCESS ALL THE TEXTUAL DATA
    parser = argparse.ArgumentParser()
    parser.add_argument("--lemm", action="store_true", help="use to apply lemmatization during preprocessing")
    parser.add_argument("--path", type=str, default="data/", help="path to the folder that contain the dataset")
    args = parser.parse_args()

    text_preprocessing = TextPreprocessing(data_path=args.path, use_lemmatizer=args.lemm)
    train_dataset, test_dataset = text_preprocessing.load_clean_dataset(clean_again=True)
