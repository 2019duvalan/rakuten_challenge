"""
This file is use to specifiy the different parameters
that will be tested for each model
"""

from sklearn.naive_bayes import GaussianNB
from sklearn.linear_model import LogisticRegression
from sklearn.tree import DecisionTreeClassifier
from sklearn.svm import SVC

import warnings
warnings.filterwarnings("ignore")

parameters_random_forest = {
        'n_estimators': [50, 100, 200],
        'max_depth': [5, 10, 15],
        'random_state': [0]
    }

parameters_decision_tree = {
        'criterion': ['gini', 'entropy'],
        'splitter': ['best'],
        'max_depth': [10, 25, 50],
        'random_state': [0]
    }

parameters_adaboost = {
        'n_estimators': [50, 100, 200],
        'learning_rate': [1, 0.1, 0.01],
        'base_estimator': [DecisionTreeClassifier(max_depth=1), DecisionTreeClassifier(max_depth=5)],
        'random_state': [0]
    }

parameters_gradient_boosting = {
        'loss': ['deviance'],
        'n_estimators': [50, 100, 200],
        'learning_rate': [1, 0.1, 0.01],
        'max_depth': [3, 10],
        'random_state': [0]
    }

parameters_bagging = {
        'base_estimator': [SVC(), LogisticRegression()],
        'n_estimators': [10, 50, 100],
        'random_state': [0]
    }

parameters_xgboost = {
        'n_estimators': [50, 100, 200],
        'max_depth': [5, 10, 15],
        'random_state': [0]
    }
