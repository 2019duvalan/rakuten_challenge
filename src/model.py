from sklearn.metrics import f1_score
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import train_test_split
import sklearn.ensemble
import sklearn.tree
import xgboost
import pandas as pd

class Model:
    """
    Class Model
    Is use to load and train a sklearn or xgboost model based on this name
    """
    def __init__(self, model_name, parameters):
        self.model_name = model_name
        self.clf = self.get_classifier(model_name)
        self.parameters = parameters
        self.grid_search = None

    def train(self, x_train, y_train, subset_ratio=1., nb_folds=3, save_result=True):
        """
        perform a gridsearch on the x_train, y_train set using :
            - self.clf as a classifier
            - self.parameters as a set of different parameter configurations

        :param x_train: [np.array | sparse.matrix] training set
        :param y_train: [np.array] : the label
        :param subset_ratio: [float] : the proportion of training to use
        :param nb_folds: [int] : the number of fold to use for cross_validation
        :param save_result: [boolean] : if True, create a file containing the different results in the result folder
        """
        _, subset_x_train, _, subset_y_train = train_test_split(x_train, y_train,
                                                                test_size=subset_ratio, random_state=42)

        self.grid_search = GridSearchCV(self.clf,
                                        cv=nb_folds,
                                        scoring='f1_weighted',
                                        return_train_score=True,
                                        param_grid=self.parameters,
                                        verbose=1)

        self.grid_search.fit(subset_x_train, subset_y_train)

        if save_result:
            result = pd.DataFrame(self.grid_search.cv_results_).T
            result.to_csv('../result/Result_GridSearch_' + self.model_name + '.csv', sep=';')

    @staticmethod
    def get_classifier(model_name):
        """
        From a given name of a classifier, search for the classifier into xgboost and scikitlearn
        library and return it
        :param model_name: string name of the classifier we want to use
        :return: the classifier
        """
        if hasattr(sklearn.ensemble, model_name):
            return getattr(sklearn.ensemble, model_name)()
        elif hasattr(xgboost, model_name):
            return getattr(xgboost, model_name)()
        elif hasattr(sklearn.tree, model_name):
            return getattr(sklearn.tree, model_name)()
        else:
            raise ImportError('{} was not found in xgboost or sklearn.ensemble.'.format(model_name))

    def get_best_params(self):
        """
        :return: the best parameters configuration that was found during grid_search
        """
        return self.grid_search.best_params_
