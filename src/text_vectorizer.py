from sklearn.feature_extraction.text import TfidfVectorizer

def vectorize_text(train, test, min_df=0.0005):
    """
    Take as input the training and test dataset
    Vectorize the string feature of each dataset using Tf-Idf
    Return x_train, x_test numpy array
    """
    vectorizer = TfidfVectorizer(min_df=min_df)

    vectorizer.fit(list(train["string"]) + list(test["string"]))
    x_train = vectorizer.transform(train["string"])
    x_test = vectorizer.transform(test["string"])

    return x_train, x_test

