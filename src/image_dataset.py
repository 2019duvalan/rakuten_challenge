from PIL import Image
import pandas as pd
import numpy as np
from torchvision import models, transforms
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm
import pickle
from scipy import sparse
import argparse

def label_to_one_hot(label):
    """
    :param label: output of VGG - np.array of shape (batch_size, nb_label=1000)
    for each input image, only keep the most propable label return it as a one hot vector

    :return: sparse matrix of shape (batch_size, nb_label)
    """
    one_hot = np.zeros(label.shape)
    most_probable = np.argmax(label, axis=1)
    one_hot[range(one_hot.shape[0]), most_probable] = 1
    return sparse.csr_matrix(one_hot)

class ImageDataset(Dataset):
    """
    Class ImageDataset
    This class is used to load on the fly the image to a torch DataLoader
    """
    def __init__(self, path, index_dataframe, img_size):
        """
        :param path: path to the folder containing the image
        :param index_dataframe: dataframe containing the imageid and productid for each image
        :param img_size: size of the image that is expected for the model input
        """
        self.path = path
        self.index_dataframe = index_dataframe
        self.transform = lambda img: transforms.ToTensor()(transforms.Resize(img_size)(img))

    def __len__(self):
        return self.index_dataframe.shape[0]

    def __getitem__(self, idx):
        """
        Reconstruct the image name using imageid and productdi
        """
        picture_name = "image_" + str(self.index_dataframe.loc[idx]['imageid']) + '_' + \
                       "product_" + str(self.index_dataframe.loc[idx]['productid']) + '.jpg'
        image = Image.open(self.path + picture_name)
        return self.transform(image)


if __name__ == "__main__":
    # SCRIPT TO GET VGG16 PROBABILITY VECTOR SCORE FOR EACH IMAGE OF THE TRAIN DATASET
    # THEN ALL THE RESULTING DATA IS DUMPED IN A PICKLE FILE

    parser = argparse.ArgumentParser()
    parser.add_argument("--data_path", type=str, default="data/", help="path to the folder that contain the dataset")
    parser.add_argument("--image_path", type=str, default="image/", help="path to the folder that contain the images")
    args = parser.parse_args()

    vgg16 = models.vgg16(pretrained=True)
    X_train = pd.read_csv(args.path + 'X_train.csv')
    image_train_dataset = ImageDataset(args.image_path + "image_train/", X_train, 224)

    train_loader = DataLoader(image_train_dataset, batch_size=100)
    images_label = []
    for batch in tqdm(train_loader):
        images_label.append(vgg16(batch).detach().numpy())

    images_label = np.vstack(images_label)
    pickle.dump(images_label, open(args.path + "images_label.pkl", "wb"))

