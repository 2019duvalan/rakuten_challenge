from text_preprocessing import TextPreprocessing
from text_vectorizer import vectorize_text
from image_dataset import label_to_one_hot
from conf_file import *
from scipy import sparse
from model import Model
import pickle
import argparse


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--lemm", action="store_true", help="to use the text preprocessed with lemmatization")
    parser.add_argument("--image", action="store_true", help="to also use the image")
    parser.add_argument("--path", type=str, default="data/", help="path to the folder that contain the dataset")
    parser.add_argument("--subset", type=float, default=0.5, help="subset of the train set to use")
    args = parser.parse_args()

    # LOAD PREPROCESS TEXTUAL DATA
    print("LOAD THE PREPROCESSED TEXTUAL DATA")
    text_preprocessing = TextPreprocessing(data_path=args.path, use_lemmatizer=args.lemm)
    train_clean, test_clean = text_preprocessing.load_clean_dataset(clean_again=False)

    # REMOVE CREATED NAN VALUE
    train_clean.fillna(value="", inplace=True)
    test_clean.fillna(value="", inplace=True)

    # VECTORIZE TEXT
    print("VECTORIZE TEXTUAL DATA")
    x_train_text, x_test_text = vectorize_text(train_clean, test_clean)

    if args.image:
        # LOAD PREPROCESS IMAGE DATA
        # EACH IMAGE WAS PASSED INSIDE VGG AND WE CHARACTIZE AN IMAGE BY THE OUTPUT LABEL
        print("LOAD VGG LABELS OF IMAGES TRAINING SET")
        x_train_picture = pickle.load(open(args.path + "images_label.pkl", "rb"))

        # ONLY KEEP THE MOST PROBABLE LABEL
        # AS DESCRIBE IN THE REPORT, THIS DOEST NOT IMPROVE THE PERFORMANCE OF THE MODEL
        x_train_picture = label_to_one_hot(x_train_picture)
        x_train = sparse.hstack([x_train_text, x_train_picture])
    else:
        x_train = x_train_text

    # CREATE LABEL
    y_train = train_clean["label"]

    # CREATE THE CLASSIFIERS
    print("CREATE THE CLASSIFIERS")
    classifiers = [("DecisionTreeClassifier", parameters_decision_tree),
                   ("AdaBoostClassifier", parameters_adaboost),
                   ("GradientBoostingClassifier", parameters_gradient_boosting),
                   ("RandomForestClassifier", parameters_random_forest),
                   ("XGBClassifier", parameters_xgboost),
                   ("BaggingClassifier", parameters_bagging)]

    models = [Model(model_tuple[0], model_tuple[1]) for model_tuple in classifiers]

    # PERFORM GRID SEARCH ON EVERY MODEL
    print("PERFORM GRID SEARCH ON EVERY MODEL")
    for model in models:
        print('TRAINING: ',model.model_name)
        model.train(x_train, y_train, subset_ratio=args.subset)


